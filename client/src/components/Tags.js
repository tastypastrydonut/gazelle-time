// @flow

import classNames from 'classnames';
import React from 'react';

import styles from './Tags.css';

const Tags = ({ className, tags }: { className?: string, tags: Array<string> }) => (
  <ul className={classNames(styles.tags, className)}>
    {tags.map((tag) => (
      <li key={tag} className={styles.tag}>{tag}</li>
    ))}
  </ul>
);

export default Tags;
