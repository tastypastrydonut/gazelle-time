const AV = require('av');
const EventEmitter = require('events');

const BufferedStream = require('./BufferedStream');

class TorrentFileSource extends EventEmitter {
  constructor(file) {
    super();

    this.file = file;
    this.loaded = 0;
    this.stream = file.createReadStream().pipe(new BufferedStream({
      length: Math.max(file.length / 10, 1024 * 750),
    }));

    this.stream.on('data', (chunk) => {
      this.loaded += chunk.length;
      this.emit('progress', this.loaded / file.length * 100);
      this.emit('data', new AV.Buffer(new Uint8Array(chunk)));
    });

    this.stream.on('error', (err) => {
      this.emit('error', err);
    });

    this.stream.on('end', () => {
      this.emit('end');
    });

    this.stream.pause();
  }

  start() { this.stream.resume(); }
  pause() { this.stream.pause(); }
  reset() { this.stream.pause(); }
}

module.exports = TorrentFileSource;
