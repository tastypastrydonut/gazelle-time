// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { torrentsAdd, torrentsSync } from 'ducks/torrents';

import PlaylistComponent from 'components/Playlist';
import Scrollable from 'components/Scrollable';

import styles from './Playlist.css';

class Playlist extends Component {
  state = { filter: null };

  componentDidMount() {
    if (this.props.torrentsSync) {
      this.props.torrentsSync();
    }
  }

  render() {
    return (
      <Scrollable
        ref={(el) => { if (el) { this.scrollableEl = el; } }}
      >
        <input
          className={styles.input}
          onChange={(ev) => { this.setState({ filter: ev.target.value }); }}
        />

        <PlaylistComponent filter={this.state.filter} />
      </Scrollable>
    );
  }

  scrollableEl: ?HTMLElement;
}

export default connect(null, { torrentsAdd, torrentsSync })(Playlist);
