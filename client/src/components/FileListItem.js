// @flow

import classNames from 'classnames';
import filesize from 'filesize';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { streamPause, streamPlay, streamStart } from 'ducks/stream';

import FontAwesome from 'components/FontAwesome';

import styles from './FileListItem.css';

class FileListItem extends Component {
  render() {
    const { infoHash, file, stream } = this.props;

    const active = stream.infoHash === infoHash && stream.fileName === file.path;

    return (
      <li className={classNames(styles.item, { [styles.activeItem]: active })}>
        <FontAwesome
          className={classNames(styles.icon, { [styles.activeIcon]: active })}
          icon={active && stream.audio && !stream.audio.paused ? 'pause' : 'play'}
          onClick={() => {
            if (!active) {
              this.props.streamStart(infoHash, file.path, file.length);
              return;
            }

            if (stream.audio && !stream.audio.paused) {
              this.props.streamPause();
            } else {
              this.props.streamPlay();
            }
          }}
        />

        <span className={styles.name}>{file.name}</span>

        {(file.downloaded < file.length) ? (
          <span className={styles.size}>{Math.min(100, file.downloaded / file.length * 100).toFixed(2)}% of {filesize(file.length)}</span>
        ) : (
          <span className={styles.size}>{filesize(file.length)}</span>
        )}
      </li>
    );
  }
}

export default connect(({ stream }) => ({ stream }), {
  streamPause,
  streamPlay,
  streamStart,
})(FileListItem);
