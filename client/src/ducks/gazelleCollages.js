import axios from 'axios';
import { createAction, handleActions } from 'redux-actions';

export const BEGIN = 'gazelle-time/gazelle/collages/BEGIN';
export const COMPLETE = 'gazelle-time/gazelle/collages/COMPLETE';
export const ERROR = 'gazelle-time/gazelle/collages/ERROR';
export const RESET = 'gazelle-time/gazelle/collages/RESET';

export const gazelleCollagesBegin = createAction(BEGIN, (term, tags) => ({ term, tags }));
export const gazelleCollagesComplete = createAction(COMPLETE, (results, meta) => ({ results, meta }));
export const gazelleCollagesError = createAction(ERROR, (error) => ({ error }));
export const gazelleCollagesReset = createAction(RESET);

export function gazelleCollages(term, tags) {
  return (dispatch) => {
    dispatch(gazelleCollagesBegin(term));

    axios.get(`${process.env.GAZELLE_URL}/collages.php?action=search&search=${term}&tags=${tags.join(',')}`).then((res) => {
      if (res.status !== 200) {
        dispatch(gazelleCollagesError(new Error(`invalid response code; expected 200 but got "${res.status}"`)));
        return;
      }

      try {
        const parser = new DOMParser();
        const doc = parser.parseFromString(res.data, 'text/html');

        const ctable = doc.querySelector('.collage_table');
        if (!ctable) {
          dispatch(gazelleCollagesError(new Error('couldn\'t find collage list table in html')));
          return;
        }

        const results = Array.from(ctable.querySelectorAll('tr')).slice(1).map((row) => {
          try {
            const cells = Array.from(row.querySelectorAll('td'));

            const category = cells[0].innerText.trim();
            const name = cells[1].querySelector('a').innerText.trim();
            const id = parseInt(cells[1].querySelector('a').getAttribute('href').replace(/^.+?(\d+)$/, '$1'), 10);
            const tags = Array.from(cells[1].querySelectorAll('.tags > a')).map((e) => e.innerText.trim());
            const torrentCount = parseInt(cells[2].innerText.trim(), 10);
            const subscriberCount = parseInt(cells[3].innerText.trim(), 10);
            const updatedAt = new Date(cells[4].querySelector('span.time').getAttribute('title')).toISOString();
            const author = cells[5].innerText.trim();

            return { id, category, name, tags, torrentCount, subscriberCount, updatedAt, author };
          } catch (err) {
            console.log(err);
            return null;
          }
        }).filter((e) => !!e);

        dispatch(gazelleCollagesComplete(results));
      } catch (err) {
        dispatch(gazelleCollagesError(err));
        return;
      }
    }, (err) => {
      dispatch(gazelleCollagesError(err));
    });
  };
}

const defaultState = {
  term: null,
  tags: null,
  loading: false,
  error: null,
  results: [],
};

export default handleActions({
  [BEGIN]: (state, action) => ({ ...state, loading: true, error: null, ...action.payload }),
  [COMPLETE]: (state, action) => ({ ...state, loading: false, error: null, ...action.payload }),
  [ERROR]: (state, action) => ({ ...state, loading: false, profile: null, error: action.payload }),
  [RESET]: defaultState,
}, defaultState);
