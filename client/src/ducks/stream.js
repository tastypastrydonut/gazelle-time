import { decode } from 'base64-arraybuffer';
import { createAction, handleActions } from 'redux-actions';
import { v4 } from 'uuid';

export const BEGIN = 'gazelle-time/stream/BEGIN';
export const AUDIO = 'gazelle-time/stream/AUDIO';
export const BUFFER = 'gazelle-time/stream/BUFFER';
export const DATA = 'gazelle-time/stream/DATA';
export const END = 'gazelle-time/stream/END';
export const RESET = 'gazelle-time/stream/RESET';
export const VOLUME = 'gazelle-time/stream/VOLUME';
export const PLAY = 'gazelle-time/stream/PLAY';
export const PAUSE = 'gazelle-time/stream/PAUSE';
export const SEEK = 'gazelle-time/stream/SEEK';
export const STOP = 'gazelle-time/stream/STOP';
export const UPDATE = 'gazelle-time/stream/UPDATE';

export const streamVolume = createAction(VOLUME, (volume) => ({ volume }));
export const streamPlay = createAction(PLAY);
export const streamPause = createAction(PAUSE);
export const streamReset = createAction(RESET);
export const streamSeek = createAction(SEEK, (time) => ({ time }));
export const streamStop = createAction(STOP, (uniqueId) => ({ uniqueId }), (uniqueId) => ({ ipc: { channel: 'stream/stop', args: [ uniqueId ] } }));

export function streamStart(infoHash, fileName, totalSize) {
  return (dispatch, getState) => {
    const previous = getState().stream;
    if (previous.uniqueId) {
      dispatch(streamStop(previous.uniqueId, previous.infoHash, previous.fileName));
    }

    const source = new MediaSource();

    const audio = new Audio();
    audio.src = URL.createObjectURL(source);

    const uniqueId = v4();

    dispatch({
      type: BEGIN,
      payload: { uniqueId, infoHash, fileName },
    });

    audio.addEventListener('canplay', () => {
      dispatch({ type: PLAY });
    });

    audio.addEventListener('loadedmetadata', () => {
      if (audio.title) {
        dispatch({ type: UPDATE, payload: { title: audio.title } });
      }
    });

    audio.addEventListener('ended', () => {
      dispatch({ type: UPDATE, payload: { paused: true } });

      const { torrents } = getState().torrents;
      if (!torrents) {
        return;
      }

      const torrentIndex = torrents.findIndex((e) => e.infoHash === infoHash);
      if (torrentIndex === -1) {
        return;
      }

      const torrent = torrents[torrentIndex];
      if (!torrent) {
        return;
      }

      const fileIndex = torrent.files.findIndex((e) => e.path === fileName);
      if (fileIndex === -1) {
        return;
      }

      let nextTorrent = torrent;
      let nextFile = torrent.files.slice(fileIndex + 1).filter((e) => e.path.match(/\.mp3$/)).shift();

      if (!nextFile) {
        nextTorrent = torrents[torrentIndex + 1];
        if (!nextTorrent) {
          nextTorrent = torrents[0];
        }

        nextFile = nextTorrent.files.filter((e) => e.path.match(/\.mp3$/)).shift();
      }

      if (nextTorrent && nextFile) {
        dispatch(streamStart(nextTorrent.infoHash, nextFile.path, nextFile.length));
      }
    });

    audio.addEventListener('durationchange', () => {
      if (audio.duration !== Infinity) {
        dispatch({ type: UPDATE, payload: { duration: audio.duration } });
      }
      if (audio.title) {
        dispatch({ type: UPDATE, payload: { title: audio.title } });
      }
    });

    audio.addEventListener('timeupdate', () => {
      dispatch({ type: UPDATE, payload: { currentTime: audio.currentTime } });
    });

    dispatch({
      type: AUDIO,
      payload: {
        audio: {
          title: fileName.split('/').pop().split('.').slice(0, -1).join('.'),
          duration: 0,
          currentTime: 0,
          volume: audio.volume,
          paused: audio.paused,
          source() { return audio; },
        },
      },
    });

    let buffer = null;

    source.addEventListener('sourceopen', () => {
      if (buffer) {
        return;
      }

      const list = [];
      let complete = false;
      let updating = false;

      buffer = source.addSourceBuffer('audio/mpeg');

      buffer.addEventListener('updateend', () => {
        updating = false;

        if (list.length > 0) {
          if (!buffer.updating) {
            updating = true;
            buffer.appendBuffer(list.shift());
          }
        } else if (complete) {
          source.endOfStream();
        }
      });

      dispatch({
        type: BUFFER,
        meta: { ipc: { channel: 'stream/start', args: [ uniqueId, infoHash, fileName ] } },
        payload: {
          buffer: {
            loaded: 0,
            total: totalSize,
            push(chunk) {
              if (updating) {
                list.push(chunk);
              } else {
                updating = true;
                buffer.appendBuffer(chunk);
              }
            },
            end() { complete = true; },
          },
        },
      });
    });
  };
}

const defaultState = {
  uniqueId: null,
  infoHash: null,
  fileName: null,
  audio: null,
  buffer: null,
};

export default handleActions({
  [BEGIN]: (state, action) => ({ ...state, ...action.payload }),
  [UPDATE]: (state, action) => ({
    ...state,
    audio: { ...state.audio, ...action.payload },
  }),
  [VOLUME]: (state, action) => {
    if (!state.audio) { return state; }
    state.audio.source().volume = action.payload.volume;
    return { ...state, audio: { ...state.audio, ...action.payload } };
  },
  [PLAY]: (state) => {
    if (!state.audio) { return state; }
    state.audio.source().play();
    return { ...state, audio: { ...state.audio, paused: false } };
  },
  [PAUSE]: (state) => {
    if (!state.audio) { return state; }
    state.audio.source().pause();
    return { ...state, audio: { ...state.audio, paused: true } };
  },
  [SEEK]: (state, action) => {
    if (!state.audio) { return state; }
    state.audio.source().currentTime = action.payload.time;
    return state;
  },
  [STOP]: (state) => {
    if (!state.audio) { return state; }
    state.audio.source().stop();
    return { ...state, audio: null, buffer: null };
  },
  [AUDIO]: (state, action) => ({ ...state, ...action.payload }),
  [BUFFER]: (state, action) => ({ ...state, ...action.payload }),
  [DATA]: (state, action) => {
    if (!state.buffer) {
      return state;
    }

    if (action.payload.uniqueId !== state.uniqueId) {
      return state;
    }

    const chunk = decode(action.payload.chunk);

    state.buffer.push(chunk);

    return {
      ...state,
      buffer: {
        ...state.buffer,
        loaded: state.buffer.loaded + chunk.byteLength,
      },
    };
  },
  [END]: (state) => {
    if (state.buffer) {
      state.buffer.end();
    }

    return state;
  },
  [STOP]: (state) => {
    if (state.audio) {
      state.audio.source().pause();
      state.audio.source().src = null;
    }

    return defaultState;
  },
  [RESET]: () => defaultState,
}, defaultState);
