const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HTMLPlugin = require('html-webpack-plugin');
const UnusedFilesWebpackPlugin = require('unused-files-webpack-plugin').UnusedFilesWebpackPlugin;

const byExtension = (...arr) => arr.reduce((r, obj) => r.concat(Object.keys(obj).map((k) => Object.assign({
  test: new RegExp(`\.(${k})$`),
}, obj[k]))), []);

module.exports = {
  devtool: 'source-map',
  entry: {
    electron: [ path.join(__dirname, 'src', 'entry-electron') ],
  },
  target: 'electron-renderer',
  output: {
    path: path.join(__dirname, '..', 'app', 'client'),
    filename: '[name]-bundle.js',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      'process.env.GAZELLE_URL': JSON.stringify(process.env.GAZELLE_URL || 'https://localhost:8080'),
    }),
  ],
  module: {
    loaders: byExtension({
      'css': {
        include: /node_modules/,
        loaders: [ 'style', 'css' ],
      },
    }, {
      'css': {
        exclude: /node_modules/,
        loaders: [
          'style',
          process.env.NODE_ENV === 'production' ? (
            'css?modules'
          ) : (
            'css?modules&localIdentName=[path][name]---[local]---[hash:base64:5]'
          ),
        ],
      },
      'woff2?(\\?v=[0-9]\\.[0-9]\\.[0-9])?': {
        loaders: [ 'url?limit=10000&mimetype=application/font-woff' ],
      },
      '(ttf|eot|svg|otf)(\\?v=[0-9]\\.[0-9]\\.[0-9])?': {
        loaders: [ 'file' ],
      },
      'js': {
        loaders: [ 'babel' ],
        include: path.join(__dirname, 'src'),
      },
      'json': {
        loaders: [ 'json' ],
      },
      'tpl': {
        loaders: [ 'template-string' ],
      },
      'jpg': {
        loaders: [ 'url?limit=10000&mimetype=image/jpeg', 'image-webpack' ],
      },
      'png': {
        loaders: [ 'url?limit=10000&mimetype=image/png', 'image-webpack' ],
      },
    }),
  },
  resolve: {
    extensions: ['', '.js'],
    alias: {
      components: path.join(__dirname, 'src', 'components'),
      containers: path.join(__dirname, 'src', 'containers'),
      ducks: path.join(__dirname, 'src', 'ducks'),
      lib: path.join(__dirname, 'src', 'lib'),
      schema: path.join(__dirname, 'src', 'schema'),
      vendor: path.join(__dirname, 'src', 'vendor'),
    },
  },
};

if (process.env.NODE_ENV === 'production') {
  module.exports.output.filename = '[name]-bundle-[hash].js';

  module.exports.plugins.push(new UnusedFilesWebpackPlugin({
    pattern: 'src/**/*.*',
    globOptions: { ignore: 'src/**/*Spec.js' },
    failOnUnused: true,
  }));

  module.exports.plugins.push(new ExtractTextPlugin('styles-[hash].css'));

  module.exports.plugins.push(new HTMLPlugin({
    title: 'Gazelle Time',
    hash: true,
    filename: 'index-electron.html',
    template: path.join(__dirname, 'src', 'index-electron.tpl'),
    chunks: 'electron',
    inject: false,
  }));

  module.exports.module.loaders.forEach((loader) => {
    if (loader.loaders.indexOf('style') === 0) {
      loader.loader = ExtractTextPlugin.extract(...loader.loaders);
      delete loader.loaders;
    }
  });
}
