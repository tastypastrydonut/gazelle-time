import { ipcRenderer } from 'electron';

export default function makeIPCMiddleware(options = {}) {
  const events = options.events || {};

  return (store) => {
    Object.keys(events).forEach((remoteEvent) => {
      ipcRenderer.on(remoteEvent, (event, payload) => {
        store.dispatch({ type: events[remoteEvent], payload });
      });
    });

    return (next) => (action) => {
      if (typeof action === 'object' && action.meta && action.meta.ipc) {
        ipcRenderer.send(action.meta.ipc.channel, action.meta.ipc.args);
      }

      return next(action);
    };
  };
}
