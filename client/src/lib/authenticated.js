import React from 'react';

import Authenticated from 'containers/Authenticated';

export default (C) => (props) => ( <Authenticated><C {...props} /></Authenticated> );
