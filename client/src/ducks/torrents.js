import { createAction, handleActions } from 'redux-actions';

export const ADD = 'gazelle-time/torrents/ADD';
export const PUSH = 'gazelle-time/torrents/PUSH';
export const RESET = 'gazelle-time/torrents/RESET';
export const SYNC = 'gazelle-time/torrents/SYNC';
export const SUBSCRIBE = 'gazelle-time/torrents/SUBSCRIBE';
export const UNSUBSCRIBE = 'gazelle-time/torrents/UNSUBSCRIBE';

export const torrentsAdd = createAction(ADD, null, (url) => ({ ipc: { channel: 'torrents/add', args: [ url ] } }));
export const torrentsPush = createAction(PUSH, (torrents) => ({ torrents }));
export const torrentsReset = createAction(RESET);
export const torrentsSync = createAction(SYNC, null, () => ({ ipc: { channel: 'torrents/sync' } }));
export const torrentsSubscribe = createAction(SUBSCRIBE, null, () => ({ ipc: { channel: 'torrents/subscribe' } }));
export const torrentsUnsubscribe = createAction(UNSUBSCRIBE, null, () => ({ ipc: { channel: 'torrents/unsubscribe' } }));

const defaultState = {
  fetching: false,
  torrents: [],
};

export default handleActions({
  [SYNC]: (state) => ({ ...state, fetching: true }),
  [PUSH]: (state, action) => ({ ...state, ...action.payload, fetching: false }),
  [RESET]: () => defaultState,
}, defaultState);
