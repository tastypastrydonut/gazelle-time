import axios from 'axios';
import { createAction, handleActions } from 'redux-actions';

export const FETCH_BEGIN = 'gazelle-time/gazelle/user/FETCH_BEGIN';
export const FETCH_COMPLETE = 'gazelle-time/gazelle/user/FETCH_COMPLETE';
export const FETCH_ERROR = 'gazelle-time/gazelle/user/FETCH_ERROR';
export const LOGIN_BEGIN = 'gazelle-time/gazelle/user/LOGIN_BEGIN';
export const LOGIN_COMPLETE = 'gazelle-time/gazelle/user/LOGIN_COMPLETE';
export const LOGIN_ERROR = 'gazelle-time/gazelle/user/LOGIN_ERROR';
export const RESET = 'gazelle-time/gazelle/user/RESET';

export const gazelleUserFetchBegin = createAction(FETCH_BEGIN);
export const gazelleUserFetchComplete = createAction(FETCH_COMPLETE, (id, authkey, profile) => ({ id, authkey, profile }));
export const gazelleUserFetchError = createAction(FETCH_ERROR, (error) => ({ error }));
export const gazelleUserLoginBegin = createAction(LOGIN_BEGIN);
export const gazelleUserLoginComplete = createAction(LOGIN_COMPLETE, (id, authkey, profile) => ({ id, authkey, profile }));
export const gazelleUserLoginError = createAction(LOGIN_ERROR, (error) => ({ error }));
export const gazelleUserReset = createAction(RESET);

export function gazelleUserLogin(username, password) {
  return (dispatch) => {
    dispatch(gazelleUserLoginBegin());

    const form = new FormData();
    form.set('username', username);
    form.set('password', password);
    form.set('keeplogged', '1');

    axios.post(`${process.env.GAZELLE_URL}/login.php`, form).then((loginRes) => {
      if (loginRes.request.responseURL === `${process.env.GAZELLE_URL}/login.php`) {
        dispatch(gazelleUserLoginError(new Error('invalid redirect path; probably incorrect username or password')));
        return;
      }

      const idMatch = loginRes.data.match(/var userid = ([0-9]+);/);
      if (!idMatch) {
        dispatch(gazelleUserLoginError(new Error('invalid response content; couldn\'t find user id')));
        return;
      }

      const id = parseInt(idMatch[1], 10);

      const authMatch = loginRes.data.match(/authkey=([0-9a-f]+)/i);
      if (!authMatch) {
        dispatch(gazelleUserLoginError(new Error('invalid response content; couldn\'t find auth key')));
        return;
      }

      axios.get(`${process.env.GAZELLE_URL}/ajax.php?action=user&id=${id}`).then((profileRes) => {
        dispatch(gazelleUserLoginComplete(id, authMatch[1], profileRes.data.response));
      }, (err) => {
        dispatch(gazelleUserLoginError(err));
      });
    }, (err) => {
      dispatch(gazelleUserLoginError(err));
    });
  };
}

export function gazelleUserFetch(id) {
  return (dispatch) => {
    dispatch(gazelleUserFetchBegin());

    axios.get(`${process.env.GAZELLE_URL}/`).then((pageRes) => {
      const authMatch = pageRes.data.match(/authkey=([0-9a-f]+)/i);
      if (!authMatch) {
        dispatch(gazelleUserFetchError(new Error('invalid response content; couldn\'t find auth key')));
        return;
      }

      axios.get(`${process.env.GAZELLE_URL}/ajax.php?action=user&id=${id}`).then((profileRes) => {
        dispatch(gazelleUserFetchComplete(id, authMatch[1], profileRes.data.response));
      }, (err) => {
        dispatch(gazelleUserFetchError(err));
      });
    });
  };
}

const defaultState = {
  loading: false,
  id: null,
  authkey: null,
  error: null,
  profile: null,
};

export default handleActions({
  [FETCH_BEGIN]: (state) => ({ ...state, loading: true, error: null, profile: null }),
  [FETCH_COMPLETE]: (state, action) => ({ ...state, loading: false, error: null, ...action.payload }),
  [FETCH_ERROR]: (state, action) => ({ ...state, loading: false, profile: null, error: action.payload }),
  [LOGIN_BEGIN]: (state) => ({ ...state, loading: true, error: null, profile: null }),
  [LOGIN_COMPLETE]: (state, action) => ({ ...state, loading: false, error: null, ...action.payload }),
  [LOGIN_ERROR]: (state, action) => ({ ...state, loading: false, profile: null, error: action.payload }),
  [RESET]: defaultState,
}, defaultState);
