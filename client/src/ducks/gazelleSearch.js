import axios from 'axios';
import { createAction, handleActions } from 'redux-actions';

export const BEGIN = 'gazelle-time/gazelle/search/BEGIN';
export const COMPLETE = 'gazelle-time/gazelle/search/COMPLETE';
export const ERROR = 'gazelle-time/gazelle/search/ERROR';
export const RESET = 'gazelle-time/gazelle/search/RESET';

export const gazelleSearchBegin = createAction(BEGIN, (term, tags, page) => ({ term, tags, page }));
export const gazelleSearchComplete = createAction(COMPLETE, (results, meta) => ({ results, meta }));
export const gazelleSearchError = createAction(ERROR, (error) => ({ error }));
export const gazelleSearchReset = createAction(RESET);

export function gazelleSearch(term, tags, page = 1) {
  return (dispatch) => {
    dispatch(gazelleSearchBegin(term, tags, page));

    axios.get(`${process.env.GAZELLE_URL}/ajax.php?action=browse&searchstr=${term}&taglist=${tags.join(',')}&page=${page}`).then((res) => {
      if (res.status !== 200) {
        dispatch(gazelleSearchError(new Error(`invalid response code; expected 200 but got "${res.status}"`)));
        return;
      }

      if (res.data.status !== 'success') {
        dispatch(gazelleSearchError(new Error(`invalid status; expected "success" but got "${res.data.status}"`)));
        return;
      }

      dispatch(gazelleSearchComplete(res.data.response.results, {
        pages: res.data.response.pages,
        currentPage: res.data.response.currentPage,
      }));
    }, (err) => {
      dispatch(gazelleSearchError(err));
    });
  };
}

const defaultState = {
  term: null,
  tags: null,
  page: null,
  loading: false,
  error: null,
  results: [],
  meta: null,
};

export default handleActions({
  [RESET]: () => defaultState,
  [BEGIN]: (state, action) => ({ ...state, loading: true, error: null, ...action.payload }),
  [ERROR]: (state, action) => ({ ...state, loading: false, profile: null, error: action.payload }),
  [COMPLETE]: (state, action) => ({
    ...state,
    loading: false,
    error: null,
    ...action.payload,
    results: state.results.concat(action.payload.results),
  }),
}, defaultState);
