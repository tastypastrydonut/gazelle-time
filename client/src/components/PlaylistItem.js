// @flow

import classNames from 'classnames';
import filesize from 'filesize';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import FileList from 'components/FileList';

import styles from './PlaylistItem.css';

class PlaylistItem extends Component {
  render() {
    const { filter, infoHash, torrent } = this.props;

    const active = torrent.infoHash === infoHash;

    return (
      <li className={classNames(styles.item, {[styles.active]: active})}>
        <div className={styles.name}>
          <span className={styles.left}>{torrent.name}</span>
          {(torrent.progress < 1) ? (
            <span>{(torrent.progress * 100).toFixed(2)}% of {filesize(torrent.length || 0)}</span>
          ) : (
            <span>{filesize(torrent.length || 0)}</span>
          )}
        </div>

        <FileList infoHash={torrent.infoHash} files={torrent.files} filter={filter} />
      </li>
    );
  }
}

export default connect(({ stream: { infoHash } }) => ({ infoHash }))(PlaylistItem);
