export function match(haystack, needle) {
  return needle.split(/\s+/).every((e) => haystack.toLowerCase().includes(e.toLowerCase()));
}
