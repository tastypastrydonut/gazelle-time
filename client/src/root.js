// @flow

import React from 'react';
import { Provider } from 'react-redux';
import { IndexRoute, Route, Router } from 'react-router';

import App from 'containers/App';
import Collage from 'containers/Collage';
import Collages from 'containers/Collages';
import Playlist from 'containers/Playlist';
import Search from 'containers/Search';
import Splash from 'containers/Splash';

import authenticated from 'lib/authenticated';

function scrollToTop() {
  window.scrollTo(0, 0);
}

const Root = ({ store, history }: { store: Object, history: Object }) => (
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={Splash} onEnter={scrollToTop} />

        <Route path="playlist" component={Playlist} onEnter={scrollToTop} />
        <Route path="search" component={authenticated(Search)} onEnter={scrollToTop} />
        <Route path="collages" component={authenticated(Collages)} onEnter={scrollToTop} />
        <Route path="collage/:id" component={authenticated(Collage)} onEnter={scrollToTop} />
      </Route>
    </Router>
  </Provider>
);

export default Root;
