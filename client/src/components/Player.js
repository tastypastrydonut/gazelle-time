import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  streamPause,
  streamPlay,
  streamSeek,
  streamStop,
  streamVolume,
} from 'ducks/stream';

import FontAwesome from 'components/FontAwesome';

import styles from './Player.css';

function duration(n) {
  const seconds = Math.floor(n);
  const minutes = Math.floor(seconds / 60);

  return `${minutes}:${('0' + (seconds % 60)).substr(-2, 2)}`;
}

class Player extends Component {
  state = { width: null };

  componentDidMount() {
    window.addEventListener('keydown', this.onKeyDown = (ev) => {
      if (ev.target !== document.body) {
        return;
      }

      const { audio } = this.props;

      if ('0123456789'.indexOf(ev.key) !== -1) {
        ev.preventDefault();
        ev.stopPropagation();

        if (!audio || !audio.duration) {
          return;
        }

        if (this.props.streamSeek) {
          const n = parseInt(ev.key, 10);
          this.props.streamSeek(audio.duration * (n / 10));
        }
      }

      if (ev.key === ' ') {
        ev.preventDefault();
        ev.stopPropagation();

        if (!audio) {
          return;
        }

        if (audio.paused) {
          if (this.props.streamPlay) { this.props.streamPlay(); }
        } else {
          if (this.props.streamPause) { this.props.streamPause(); }
        }
      }

      if (ev.key === 'ArrowLeft') {
        ev.preventDefault();
        ev.stopPropagation();

        if (!audio || !audio.duration || typeof audio.currentTime !== 'number') {
          return;
        }

        if (this.props.streamSeek) {
          this.props.streamSeek(Math.max(audio.currentTime - 10, 0));
        }
      }

      if (ev.key === 'ArrowRight') {
        ev.preventDefault();
        ev.stopPropagation();

        if (!audio || !audio.duration || typeof audio.currentTime !== 'number') {
          return;
        }

        if (this.props.streamSeek) {
          this.props.streamSeek(Math.min(audio.currentTime + 10, audio.duration));
        }
      }
    }, true);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.onKeyDown);
  }

  render() {
    const { fileName, audio, buffer } = this.props;

    let progress = null;
    if (audio) {
      if (typeof audio.duration === 'number' && typeof audio.currentTime === 'number') {
        progress = (audio.currentTime / audio.duration) * 100;
      }
    }

    let buffered = null;
    if (buffer) {
      if (typeof buffer.total === 'number' && typeof buffer.loaded === 'number') {
        buffered = (buffer.loaded / buffer.total) * 100;
      }
    }

    let title = 'nothing playing';

    if (fileName) {
      title = fileName.split('/').pop().split('.').slice(0, -1).join('.');
    }

    if (audio) {
      if (typeof audio.duration === 'number' && typeof audio.currentTime === 'number') {
        title += ` (${duration(audio.currentTime)} / ${duration(audio.duration)})`;
      }
    }

    if (buffer) {
      if (typeof buffer.total === 'number' && typeof buffer.loaded !== 'number' && buffer.loaded !== buffer.total) {
        title += ` [${(buffer.loaded / buffer.total * 100).toFixed(2)}%]`;
      }
    }

    return (
      <div className={styles.player}>
        <div className={styles.icons}>
          <FontAwesome
            className={styles.icon}
            icon={(audio && !audio.paused) ? 'pause' : 'play'}
            onClick={() => {
              if (audio) {
                if (audio.paused) {
                  if (this.props.streamPlay) { this.props.streamPlay(); }
                } else {
                  if (this.props.streamPause) { this.props.streamPause(); }
                }
              }
            }}
          />

          <FontAwesome
            className={styles.icon}
            icon="stop"
            onClick={() => { if (this.props.streamStop) { this.props.streamStop(); } }}
          />
        </div>

        <div
          className={styles.title}
          onClick={(ev) => {
            if (!this.state.width || !audio || !audio.duration) {
              return;
            }

            if (this.props.streamSeek) {
              this.props.streamSeek(audio.duration * (ev.nativeEvent.layerX / this.state.width));
            }
          }}
          ref={(el) => {
            if (el && el.offsetWidth !== this.state.width) {
              this.setState({ width: el.offsetWidth });
            }
          }}
        >
          {progress ? <div className={styles.progress} style={{ width: `${progress}%` }} /> : null}
          {buffered ? <div className={styles.buffered} style={{ width: `${buffered}%` }} /> : null}

          {title}
        </div>
      </div>
    );
  }
}

export default connect(({ stream: { fileName, audio, buffer } }) => ({ fileName, audio, buffer }), {
  streamPause,
  streamPlay,
  streamSeek,
  streamStop,
  streamVolume,
})(Player);
