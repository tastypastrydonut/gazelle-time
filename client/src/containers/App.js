// @flow

import React from 'react';
import { Link } from 'react-router';

import 'vendor/source-sans-pro/source-sans-pro.css';

import Player from 'components/Player';

import styles from './App.css';

import logoURL from './AppLogo.svg';

const App = ({ children }: { children: React.Children }) => (
  <div className={styles.wrapper}>
    <Link to="/">
      <img className={styles.header} src={logoURL} />
    </Link>

    <Player />

    <ul className={styles.menu}>
      <li className={styles.menuItem}>
        <Link className={styles.menuLink} to="/playlist">Playlist</Link>
      </li>
      <li className={styles.menuItem}>
        <Link className={styles.menuLink} to="/search">Search</Link>
      </li>
      <li className={styles.menuItem}>
        <Link className={styles.menuLink} to="/collages">Collages</Link>
      </li>
    </ul>

    <div className={styles.content}>{ children }</div>
  </div>
);

export default App;
