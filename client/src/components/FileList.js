// @flow

import React, { Component } from 'react';

import FileListItem from 'components/FileListItem';

import styles from './FileList.css';

export default class FileList extends Component {
  render() {
    const { infoHash, files } = this.props;

    return (
      <ul className={styles.list}>
        {files.filter((file) => file.path.match(/mp3$/)).map((file) => (
          <FileListItem key={file.path} infoHash={infoHash} file={file} />
        ))}
      </ul>
    );
  }
}
