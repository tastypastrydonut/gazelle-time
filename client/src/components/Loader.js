/* @flow */

import classNames from 'classnames';
import React from 'react';

import FontAwesome from 'components/FontAwesome';

import styles from './Loader.css';

const Loader = (props: { className?: string }) => (
  <FontAwesome
    icon="circle-o-notch"
    className={classNames(styles.spinner, props.className)}
  />
);

export default Loader;
