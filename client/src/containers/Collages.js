// @flow

import { decode } from 'ent';
import pluralize from 'pluralize';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import timeago from 'timeago.js';

import { gazelleCollages } from 'ducks/gazelleCollages';
import { torrentsAdd } from 'ducks/torrents';

import Loader from 'components/Loader';
import Scrollable from 'components/Scrollable';
import Tags from 'components/Tags';

import styles from './Collages.css';

class Collages extends Component {
  render() {
    return (
      <div className={styles.container}>
        <form
          className={styles.form}
          onSubmit={(ev) => {
            if (ev) {
              ev.preventDefault();
            }

            if (this.props.gazelleCollages && this.searchInputEl && this.tagsInputEl) {
              this.props.gazelleCollages(this.searchInputEl.value, this.tagsInputEl.value.split(',').map((e) => e.trim()));
            }
          }}
        >
          <input
            className={styles.input}
            type="text"
            ref={(el) => { this.searchInputEl = el; }}
            placeholder="Search terms"
          />

          <input
            className={styles.input}
            type="text"
            ref={(el) => { this.tagsInputEl = el; }}
            placeholder="Tags (comma-separated)"
          />

          <input className={styles.button} type="submit" value="Search" />
        </form>

        <Scrollable>
          {this.props.loading ? <Loader /> : this.props.results.map((result) => (
            <div key={result.id} className={styles.row}>
              <div className={styles.top}>
                <div className={styles.left}>
                  <span className={styles.detail}>[{decode(result.category || '')}]</span>
                  <Link className={styles.name} to={`/collage/${result.id}`}>{decode(result.name || '')}</Link>
                  <span className={styles.detail}>({result.torrentCount} {pluralize('torrent', result.torrentCount)})</span>
                </div>
                <div className={styles.right}>
                  {(new timeago()).format(result.updatedAt)} by {decode(result.author)}
                </div>
              </div>

              <Tags tags={result.tags} />
            </div>
          ))}
        </Scrollable>
      </div>
    );
  }

  searchInputEl: ?HTMLInputElement;
  tagsInputEl: ?HTMLInputElement;
}

export default connect(({
  gazelle: { collages: { term, loading, error, results } },
}) => ({ term, loading, error, results }), {
  gazelleCollages,
  torrentsAdd,
})(Collages);
