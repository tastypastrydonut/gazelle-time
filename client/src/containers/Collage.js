// @flow

import { decode } from 'ent';
import filesize from 'filesize';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { gazelleCollageFetch } from 'ducks/gazelleCollage';
import { torrentsAdd } from 'ducks/torrents';

import FontAwesome from 'components/FontAwesome';
import Loader from 'components/Loader';
import ReleaseList from 'components/ReleaseList';
import Scrollable from 'components/Scrollable';
import Tags from 'components/Tags';

import styles from './Collage.css';

function makeURL(torrentId, authkey, passkey) {
  return `${process.env.GAZELLE_URL}/torrents.php?action=download&id=${torrentId}&authkey=${authkey}&torrent_pass=${passkey}`;
}

class Collage extends Component {
  componentDidMount() {
    if (this.props.gazelleCollageFetch) {
      this.props.gazelleCollageFetch(parseInt(this.props.params.id, 10));
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params.id !== this.props.params.id) {
      if (this.props.gazelleCollageFetch) {
        this.props.gazelleCollageFetch(parseInt(nextProps.params.id, 10));
      }
    }
  }

  render() {
    const { loading, error, collage } = this.props;

    if (!this.props.collage) {
      if (loading) {
        return (
          <div className={styles.container}>
            <Loader />
          </div>
        );
      }

      if (error) {
        return (
          <div className={styles.container}>
            Sorry, there was an error: {String(this.props.error)}
          </div>
        );
      }

      return (
        <div className={styles.container}>
          Sorry, this collage doesn't seem to exist.
        </div>
      );
    }

    return (
      <div className={styles.container}>
        <Scrollable>
          <h1 className={styles.heading}>{collage.name}</h1>

          <div className={styles.covers}>
            {collage.torrentgroups.map((group) => (
              <img
                key={group.id}
                className={styles.cover}
                src={group.wikiImage}
                title={group.name}
              />
            ))}
          </div>

          <ReleaseList
            releases={collage.torrentgroups.map((group) => ({
              ...group,
              tags: group.tagList.replace(/_/g, '.').split(' '),
            }))}
          />

          {[].map((group, i) => (
            <div key={group.id} className={styles.row}>
              <div className={styles.top}>
                <div className={styles.left}>
                  <ul className={styles.artists}>
                    {(group.musicInfo.artists.length >= 3) ? (
                      <li className={styles.artist}>Various Artists</li>
                    ) : group.musicInfo.artists.map((e) => (
                      <li key={e.id} className={styles.artist}>{e.name}</li>
                    ))}
                  </ul>

                  <span className={styles.separator}>-</span>

                  <span>{decode(group.name)} [{group.year}]</span>
                </div>

                <div className={styles.right}>{i + 1}</div>
              </div>

              <Tags className={styles.inner} tags={group.tagList.replace(/_/g, '.').split(' ')} />

              <ul className={styles.releaseList}>
                {group.torrents.map((torrent) => (
                  <li key={torrent.torrentid} className={styles.release}>
                    <div className={styles.actions}>
                      <span className={styles.action}>
                        <a href={makeURL(torrent.torrentid, this.props.authkey, this.props.passkey)}>
                          <FontAwesome icon="save" title="Download .torrent file" />
                        </a>
                      </span>
                      <span className={styles.spacer}>|</span>
                      <span className={styles.action}>
                        {torrent.format === 'MP3' ? (
                          <a
                            href="#"
                            onClick={(ev) => {
                              if (ev) { ev.preventDefault(); }

                              if (this.props.torrentsAdd) {
                                this.props.torrentsAdd(makeURL(torrent.torrentid, this.props.authkey, this.props.passkey));
                              }
                            }}
                          >
                            <FontAwesome icon="play" title="Add to playlist" />
                          </a>
                        ) : (
                          <FontAwesome icon="close" title="Can not add to playlist" />
                        )}
                      </span>
                    </div>
                    <div className={styles.format}>{torrent.media} / {torrent.format} / {torrent.encoding}</div>
                    <div className={styles.details}>{torrent.seeders}S {torrent.leechers}L {filesize(torrent.size || 0)}</div>
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </Scrollable>
      </div>
    );
  }
}

export default connect(({
  gazelle: {
    collage: { id, loading, error, collage },
    user: { authkey, profile: { personal: { passkey } } },
  },
}) => ({ id, loading, error, collage, authkey, passkey }), {
  gazelleCollageFetch,
  torrentsAdd,
})(Collage);
