import 'electron-cookies';
import ReactPerf from 'react-addons-perf';
import { hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import { setupRoot, setupStore } from './setup';

if (process.env.NODE_ENV !== 'production') {
  window.ReactPerf = ReactPerf;
}

function main() {
  const store = setupStore();
  setupRoot(document.getElementById, store, syncHistoryWithStore(hashHistory, store));
}

if (document.readyState === 'complete') {
  main();
} else {
  document.addEventListener('DOMContentLoaded', main);
}
