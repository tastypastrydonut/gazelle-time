// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { gazelleSearch, gazelleSearchReset } from 'ducks/gazelleSearch';

import Loader from 'components/Loader';
import ReleaseList from 'components/ReleaseList';
import Scrollable from 'components/Scrollable';

import styles from './Search.css';

class Search extends Component {
  render() {
    return (
      <div className={styles.container}>
        <form
          className={styles.form}
          onSubmit={(ev) => {
            if (ev) {
              ev.preventDefault();
            }

            if (this.props.gazelleSearchReset && this.props.gazelleSearch && this.searchInputEl && this.tagsInputEl) {
              const searchInputValue = this.searchInputEl.value;
              const tagsInputValue = this.tagsInputEl.value;
              this.props.gazelleSearchReset();
              this.props.gazelleSearch(searchInputValue, tagsInputValue.split(',').map((e) => e.trim()));
            }
          }}
        >
          <input
            className={styles.input}
            type="text"
            ref={(el) => { this.searchInputEl = el; }}
            placeholder="Search terms"
            defaultValue={this.props.term}
          />

          <input
            className={styles.input}
            type="text"
            ref={(el) => { this.tagsInputEl = el; }}
            placeholder="Tags (comma-separated)"
            defaultValue={(this.props.tags || []).join(', ')}
          />

          <input className={styles.button} type="submit" value="Search" />
        </form>

        {(this.props.results.length > 0) ? (
          <Scrollable
            onHitBottom={() => {
              if (this.props.meta && this.props.page >= this.props.meta.pages) {
                return;
              }

              if (!this.props.loading) {
                this.props.gazelleSearch(this.props.term, this.props.tags, this.props.page + 1);
              }
            }}
          >
            <ReleaseList releases={this.props.results} />
          </Scrollable>
        ) : null}

        {this.props.loading ? ( <Loader /> ) : null}
      </div>
    );
  }

  searchInputEl: ?HTMLInputElement;
  tagsInputEl: ?HTMLInputElement;
}

export default connect(({
  gazelle: { search: { term, tags, page, loading, error, results, meta } },
}) => ({ term, tags, page, loading, error, results, meta }), { gazelleSearch, gazelleSearchReset })(Search);
