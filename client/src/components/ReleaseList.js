// @flow

import React, { Component } from 'react';

import ReleaseListItem from 'components/ReleaseListItem';

export default class ReleaseList extends Component {
  render() {
    const { releases } = this.props;

    return (
      <div>
        {releases.map((release) => (
          <ReleaseListItem key={release.groupId} release={release} />
        ))}
      </div>
    );
  }
}
