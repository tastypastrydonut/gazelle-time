// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { match } from 'lib/match';

import { torrentsSubscribe, torrentsUnsubscribe } from 'ducks/torrents';

import PlaylistItem from 'components/PlaylistItem';

import styles from './Playlist.css';

class Playlist extends Component {
  componentDidMount() {
    if (this.props.torrentsSubscribe) {
      this.props.torrentsSubscribe();
    }
  }

  componentWillUnmount() {
    if (this.props.torrentsUnsubscribe) {
      this.props.torrentsUnsubscribe();
    }
  }

  render() {
    const { filter, torrents } = this.props;

    return (
      <ul className={styles.list}>
        {torrents.filter((torrent) => {
          if (!filter) {
            return true;
          }

          if (match(torrent.name, filter)) {
            return true;
          }

          return torrent.files.some((file) => match(file.name, filter));
        }).map((torrent) => (
          <PlaylistItem key={torrent.infoHash} torrent={torrent} filter={filter} />
        ))}
      </ul>
    );
  }
}

export default connect(({ torrents: { torrents } }) => ({ torrents }), {
  torrentsSubscribe,
  torrentsUnsubscribe,
})(Playlist);
