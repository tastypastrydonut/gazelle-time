import { combineReducers } from 'redux';

import collage from './gazelleCollage';
import collages from './gazelleCollages';
import search from './gazelleSearch';
import user from './gazelleUser';

export default combineReducers({ collage, collages, search, user });
