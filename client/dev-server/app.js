const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('../webpack.config');

const PORT = process.env.PORT || 3000;

config.entry.electron.unshift(
  'react-hot-loader/patch',
  'webpack-dev-server/client',
  'webpack/hot/only-dev-server'
);

config.plugins.unshift(new webpack.HotModuleReplacementPlugin());

config.module.loaders.forEach((loader) => {
  if (loader.loaders.includes('css?modules')) {
    loader.loaders[1] += '&localIdentName=[path][name]---[local]---[hash:base64:5]';
  }
});

new WebpackDevServer(webpack(config), {
  contentBase: __dirname,
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
}).listen(PORT, (err) => {
  if (err) {
    // crash
    throw err;
  } else {
    console.log('server started');
  }
});
