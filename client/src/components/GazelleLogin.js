import React, { Component } from 'react';
import { connect } from 'react-redux';

import { gazelleUserLogin } from 'ducks/gazelleUser';

import Loader from 'components/Loader';

import styles from './GazelleLogin.css';

class GazelleLogin extends Component {
  render() {
    const { loading, error } = this.props;

    if (loading) {
      return (
        <div className={styles.container}>
          <Loader />
        </div>
      );
    }

    return (
      <div className={styles.container}>
        <form
          className={styles.form}
          onSubmit={(ev?: DOMEvent) => {
            if (ev) { ev.preventDefault(); }

            if (this.props.gazelleUserLogin) {
              this.props.gazelleUserLogin(this.usernameEl.value, this.passwordEl.value);
            }
          }}
        >
          <div>
            <h3>Please log in with your gazelle account</h3>

            {error ? (
              <div className={styles.error}>Error: {error.message}</div>
            ) : null}

            <div className={styles.row}>
              <label
                className={styles.label}
                htmlFor={styles.username}
              >
                Username:
              </label>

              <input
                id={styles.username}
                className={styles.input}
                type="text"
                ref={(el) => { this.usernameEl = el; }}
              />
            </div>

            <div className={styles.row}>
              <label
                className={styles.label}
                htmlFor={styles.password}
              >
                Password:
              </label>

              <input
                id={styles.password}
                className={styles.input}
                type="password"
                ref={(el) => { this.passwordEl = el; }}
              />
            </div>

            <input className={styles.submit} type="submit" value="Log in" />
          </div>
        </form>
      </div>
    );
  }
}

export default connect(({ gazelle: { user: { loading, profile, error } } }) => ({ loading, profile, error }), {
  gazelleUserLogin,
})(GazelleLogin);
