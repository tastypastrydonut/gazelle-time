// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';

import GazelleLogin from 'components/GazelleLogin';

class Authenticated extends Component {
  render() {
    if (!this.props.profile) {
      return ( <GazelleLogin /> );
    }

    return this.props.children;
  }
}

export default connect(({ gazelle: { user: { profile } } }) => ({ profile }))(Authenticated);
