import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { routerReducer } from 'react-router-redux';
import Redbox from 'redbox-react';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import immutableState from 'redux-immutable-state-invariant';
import persistState from 'redux-localstorage';
import createLogger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

import makeIPCMiddleware from 'lib/ipcMiddleware';

import * as ducks from 'ducks';
import { gazelleUserFetch, gazelleUserReset } from 'ducks/gazelleUser';
import { DATA as STREAM_DATA, END as STREAM_END } from 'ducks/stream';
import { PUSH as TORRENTS_PUSH } from 'ducks/torrents';

import Root from './root';

export function setupStore() {
  let middleware = [
    thunkMiddleware,
    makeIPCMiddleware({
      events: {
        'stream/data': STREAM_DATA,
        'stream/end': STREAM_END,
        'torrents/push': TORRENTS_PUSH,
      },
    }),
  ];

  if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test') {
    middleware = [
      ...middleware,
      createLogger({
        diff: true,
        predicate: (getState, action) => {
          switch (action.type) {
          case 'gazelle-time/stream/UPDATE': return false;
          case 'gazelle-time/torrents/PUSH': return false;
          default: return true;
          }
        },
      }),
      // immutableState(),
    ];
  }

  const createStoreWithMiddleware = compose(
    persistState(null, {
      slicer: () => (state) => ({
        gazelle: {
          user: {
            id: state.gazelle.user.id,
          },
        },
      }),
    }),
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : (s) => s
  )(createStore);

  const store = createStoreWithMiddleware(combineReducers({
    ...ducks,
    routing: routerReducer,
  }), {});

  if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test') {
    if (module.hot) {
      module.hot.accept('./ducks', () => {
        store.replaceReducer(combineReducers({
          ...require('./ducks'),
          routing: routerReducer,
        }));
      });
    }
  }

  try {
    const id = store.getState().gazelle.user.id;
    if (id) {
      store.dispatch(gazelleUserFetch(id));
    }
  } catch (e) {
    store.dispatch(gazelleUserReset());
  }

  return store;
}

export function setupRoot(el, store, history) {
  const reactRoot = document.getElementById('react-root');
  render(
    <AppContainer errorReporter={Redbox}>
      <Root store={store} history={history} />
    </AppContainer>,
    reactRoot
  );

  if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test') {
    if (module.hot) {
      module.hot.accept('./root', () => {
        const NextApp = require('./root').default;

        render(
          <AppContainer errorReporter={Redbox}>
             <NextApp />
          </AppContainer>,
          reactRoot
        );
      });
    }
  }
}
