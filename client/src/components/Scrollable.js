import classNames from 'classnames';
import React, { Component } from 'react';

import styles from './Scrollable.css';

export default class Scrollable extends Component {
  constructor() {
    super();

    this.state = {
      atTop: true,
      atBottom: true,
    };
  }

  render() {
    return (
      <div className={classNames(styles.container, this.props.className)}>
        <div
          className={classNames(styles.inner, this.props.innerClassName)}
          ref={(el) => { if (el) { this.handleScroll(el); } }}
          onScroll={(ev) => { this.handleScroll(ev.target); }}
        >
          {this.props.children}
        </div>

        {!this.state.atTop ? (
          <div className={styles.fadeTop} />
        ) : null}

        {!this.state.atBottom ? (
          <div className={styles.fadeBottom} />
        ) : null}
      </div>
    );
  }

  handleScroll(el) {
    if (el.scrollTop >= el.scrollHeight - el.offsetHeight - 5) {
      if (!this.state.atBottom) {
        this.setState({ atBottom: true }, this.props.onHitBottom);
      }
    } else if (this.state.atBottom) {
      this.setState({ atBottom: false }, this.props.onLeaveBottom);
    }

    if (el.scrollTop <= 5) {
      if (!this.state.atTop) {
        this.setState({ atTop: true }, this.props.onHitTop);
      }
    } else if (this.state.atTop) {
      this.setState({ atTop: false }, this.props.onLeaveTop);
    }
  }
}
