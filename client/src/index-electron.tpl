<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>${scope.htmlWebpackPlugin.options.title}</title>

    ${(scope.htmlWebpackPlugin.files.chunks.electron.css || []).map((f) => {
      return `<link rel="stylesheet" href="${f}">`;
    }).join('\n')}
  </head>
  <body>
    <div id="react-root"></div>
    <script src="${scope.htmlWebpackPlugin.files.chunks.electron.entry}"></script>
  </body>
</html>
