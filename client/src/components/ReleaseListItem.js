// @flow

import { decode } from 'ent';
import filesize from 'filesize';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { torrentsAdd } from 'ducks/torrents';

import Tags from 'components/Tags';

import styles from './ReleaseListItem.css';

function makeURL(torrentId, authkey, passkey) {
  return `${process.env.GAZELLE_URL}/torrents.php?action=download&id=${torrentId}&authkey=${authkey}&torrent_pass=${passkey}`;
}

class ReleaseListItem extends Component {
  render() {
    const { release } = this.props;

    return (
      <div key={release.groupId} className={styles.row}>
        <div>
          <div className={styles.top}>
            <div className={styles.left}>
              {decode(release.artist || '')} - {decode(release.groupName || '')} [{release.groupYear}] [{release.releaseType}]
            </div>
            <div className={styles.right}>{filesize(release.maxSize || 0)} (Max)</div>
          </div>
          <Tags className={styles.tags} tags={release.tags} />
        </div>

        <ul className={styles.releaseList}>
          {(release.torrents || []).map((torrent) => (
            <li key={torrent.torrentId} className={styles.release}>
              <div className={styles.actions}>
                <span className={styles.action}>{torrent.format === 'MP3' ? (
                  <a
                    href="#"
                    onClick={(ev) => {
                      if (ev) { ev.preventDefault(); }

                      if (this.props.torrentsAdd) {
                        this.props.torrentsAdd(makeURL(torrent.torrentId, this.props.authkey, this.props.passkey));
                      }
                    }}
                  >
                    PL
                  </a>
                ) : <strike>PL</strike>}</span>
              </div>
              <div className={styles.format}>{torrent.media} / {torrent.format} / {torrent.encoding}</div>
              <div className={styles.details}>{torrent.seeders}S {torrent.leechers}L {filesize(torrent.size || 0)}</div>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default connect(({
  gazelle: { user: { authkey, profile: { personal: { passkey } } } },
}) => ({ authkey, passkey }), { torrentsAdd })(ReleaseListItem);
