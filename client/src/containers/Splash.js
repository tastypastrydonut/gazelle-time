// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';

import Scrollable from 'components/Scrollable';

import styles from './Splash.css';

class Splash extends Component {
  render() {
    return (
      <Scrollable>
        <div className={styles.content}>
          <h2>Welcome{this.props.profile ? `, ${this.props.profile.username}` : ''}!</h2>

          <p className={styles.text}>
            Hello and welcome to the beta program. This is experimental
            software and should be treated as such. If you encounter any
            problems while using this software, please report them through the
            appropriate channels.
          </p>

          <h2>Getting Started</h2>

          <h3>Player</h3>

          <p className={styles.text}>
            The player up the top of the application does mostly what you'd
            expect. Click pause, it'll pause. Click stop, it'll stop. Once a
            track can be seeked (i.e. when it's completely downloaded and
            ready), you can click in the track title area to seek. It also has
            some shortcuts that can be used within the application.

            <ul className={styles.list}>
              <li><strong>Space</strong> - pause/play</li>
              <li><strong>Left Arrow</strong> - skip back 10 seconds</li>
              <li><strong>Right Arrow</strong> - skip forward 10 seconds</li>
              <li><strong>0-9</strong> - skip to a section of the current track</li>
            </ul>
          </p>

          <h3>Playlist</h3>

          <p className={styles.text}>
            The "playlist" section is a combined torrent client and playlist.
            Files and torrents that have not completed downloading will have a
            percentage displayed. Files and torrents that are complete will
            just have their sizes listed. The text box at the top of this
            section can be used to search for a particular torrent. It
            searches the name of the torrent and the files within.
          </p>

          <h3>Search</h3>

          <p className={styles.text}>
            The "search" section is a very cut-down version of the torrent
            search from the website. Currently it is missing <em>many</em> of
            the features that the website has. You can search by title and/or
            tag. To get to the next page, just keep scrolling down. More
            results will be loaded in when you hit the bottom if they're
            available.
          </p>

          <h3>Collages</h3>

          <p className={styles.text}>
            Like the "search" section, this is a very stripped down version of
            the collage functionality from the website. You can search by
            title and/or tag, and you can view the covers and torrents from a
            collage. Currently descriptions are not displayed.
          </p>
        </div>
      </Scrollable>
    );
  }
}

export default connect(({ gazelle: { user: { profile } } }) => ({ profile }))(Splash);
