all: dist/gazelle-time-0.0.1-x86_64.AppImage dist/mac/gazelle-time-0.0.1.dmg dist/gazelle-time\ Setup\ 0.0.1.exe

.PHONY: all clean

app/client/index-electron.html: package.json client/webpack.config.js $(shell find client/src -type f)
	yarn run build-client

dist/gazelle-time-0.0.1-x86_64.AppImage: package.json app/index.js $(shell find app/lib -type f) app/client/index-electron.html
	yarn run dist:linux

dist/mac/gazelle-time-0.0.1.dmg: package.json app/index.js $(shell find app/lib -type f) app/client/index-electron.html
	yarn run dist:macos

dist/gazelle-time\ Setup\ 0.0.1.exe: package.json app/index.js $(shell find app/lib -type f) app/client/index-electron.html
	yarn run dist:windows

urls: urls-linux urls-macos urls-windows
	cat urls-linux urls-macos urls-windows > urls

urls-linux: dist/gazelle-time-0.0.1-x86_64.AppImage
	curl --silent --output urls-linux --upload-file "dist/gazelle-time-0.0.1-x86_64.AppImage" "https://transfer.sh/gazelle-time-0.0.1-x86_64.appimage"

urls-macos: dist/mac/gazelle-time-0.0.1.dmg
	curl --silent --output urls-macos --upload-file "dist/mac/gazelle-time-0.0.1.dmg" "https://transfer.sh/gazelle-time-0.0.1.dmg"

urls-windows: dist/gazelle-time\ Setup\ 0.0.1.exe
	curl --silent --output urls-windows --upload-file "dist/gazelle-time Setup 0.0.1.exe" "https://transfer.sh/gazelle-time-setup-0.0.1.exe"

clean:
	rm -rvf dist/* app/client
