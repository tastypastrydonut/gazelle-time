import axios from 'axios';
import { decode } from 'ent';
import { createAction, handleActions } from 'redux-actions';

export const FETCH_BEGIN = 'gazelle-time/gazelle/collage/FETCH_BEGIN';
export const FETCH_COMPLETE = 'gazelle-time/gazelle/collage/FETCH_COMPLETE';
export const FETCH_ERROR = 'gazelle-time/gazelle/collage/FETCH_ERROR';
export const RESET = 'gazelle-time/gazelle/collage/RESET';

export const gazelleCollageFetchBegin = createAction(FETCH_BEGIN, (id) => ({ id }));
export const gazelleCollageFetchComplete = createAction(FETCH_COMPLETE, (collage) => ({ collage }));
export const gazelleCollageFetchError = createAction(FETCH_ERROR, (error) => ({ error }));
export const gazelleCollageFetchReset = createAction(RESET);

const releaseTypes = {
  '1': 'Album',
  '3': 'Soundtrack',
  '5': 'EP',
  '6': 'Anthology',
  '7': 'Compilation',
  '9': 'Single',
  '11': 'Live album',
  '13': 'Remix',
  '14': 'Bootleg',
  '15': 'Interview',
  '16': 'Mixtape',
  '21': 'Unknown',
};

export function gazelleCollageFetch(id) {
  return (dispatch) => {
    dispatch(gazelleCollageFetchBegin(id));

    axios.get(`${process.env.GAZELLE_URL}/ajax.php?action=collage&id=${id}`).then((res) => {
      if (res.status !== 200) {
        dispatch(gazelleCollageFetchError(new Error(`invalid response code; expected 200 but got "${res.status}"`)));
        return;
      }

      if (res.data.status !== 'success') {
        dispatch(gazelleCollageFetchError(new Error(`invalid status; expected "success" but got "${res.data.status}"`)));
        return;
      }

      const collage = res.data.response;

      collage.name = decode(collage.name);

      if (Array.isArray(collage.torrentgroups)) {
        collage.torrentgroups.forEach((group) => {
          group.name = decode(group.name);

          if (releaseTypes[group.releaseType]) {
            group.releaseType = releaseTypes[group.releaseType];
          } else {
            group.releaseType = `Unknown (${group.releaseType})`;
          }

          group.groupName = group.name || `Unknown (${group.id})`;
          group.groupYear = group.year || `Unknown (${group.id})`;

          if (Array.isArray(group.torrents)) {
            group.torrents.forEach((torrent) => {
              if (torrent.torrentid) {
                torrent.torrentId = torrent.torrentid;
              }
            });
          }

          const artistNames = group.musicInfo.artists.map((e) => e.name);

          switch (artistNames.length) {
          case 0:
            group.artist = 'Unknown';
            break;
          case 1:
            group.artist = artistNames[0];
            break;
          case 2:
            group.artist = `${artistNames[0]} and ${artistNames[1]}`;
            break;
          case 3:
            group.artist = `${artistNames[0]}, ${artistNames[1]}, and ${artistNames[2]}`;
            break;
          default:
            group.artist = 'Various Artists';
            break;
          }
        });
      }

      dispatch(gazelleCollageFetchComplete(collage));
    }, (err) => {
      dispatch(gazelleCollageFetchError(err));
    });
  };
}

const defaultState = {
  id: null,
  loading: false,
  error: null,
  collage: null,
};

export default handleActions({
  [FETCH_BEGIN]: (state, action) => ({ ...state, loading: true, error: null, collage: null, ...action.payload }),
  [FETCH_COMPLETE]: (state, action) => ({ ...state, loading: false, error: null, ...action.payload }),
  [FETCH_ERROR]: (state, action) => ({ ...state, loading: false, collage: null, error: action.payload }),
  [RESET]: defaultState,
}, defaultState);
