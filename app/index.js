#!/usr/bin/env node

const { app, BrowserWindow, Menu, ipcMain } = require('electron');
const fs = require('fs');
const path = require('path');
const throttle = require('throttle');
const url = require('url');

const WebTorrent = require('webtorrent');

const torrentClient = new WebTorrent();

let win = null;
function createWindow() {
  if (win) { return; }

  win = new BrowserWindow({
    title: 'Gazelle Time',
    width: process.env.NODE_ENV === 'development' ? 1280 : 600,
    webPreferences: { webSecurity: false },
  });

  win.once('closed', () => { win = null; });

  if (process.env.NODE_ENV === 'development') {
    setTimeout(() => {
      win.loadURL(`http://127.0.0.1:${process.env.PORT || 3000}/`);
      win.webContents.openDevTools();
    }, 1000);
  } else {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'client', 'index-electron.html'),
      protocol: 'file:',
      slashes: true,
    }));
  }
}

app.on('ready', () => {
  createWindow();

  if (process.env.NODE_ENV !== 'development') {
    Menu.setApplicationMenu(Menu.buildFromTemplate([
      {
        label: 'Application',
        submenu: [
          { label: 'About Application', selector: 'orderFrontStandardAboutPanel:' },
          { type: 'separator' },
          { label: 'Quit', accelerator: 'Command+Q', click: function() { app.quit(); }},
        ],
      },
      {
        label: 'Edit',
        submenu: [
          { label: 'Undo', accelerator: 'CmdOrCtrl+Z', selector: 'undo:' },
          { label: 'Redo', accelerator: 'Shift+CmdOrCtrl+Z', selector: 'redo:' },
          { type: 'separator' },
          { label: 'Cut', accelerator: 'CmdOrCtrl+X', selector: 'cut:' },
          { label: 'Copy', accelerator: 'CmdOrCtrl+C', selector: 'copy:' },
          { label: 'Paste', accelerator: 'CmdOrCtrl+V', selector: 'paste:' },
          { label: 'Select All', accelerator: 'CmdOrCtrl+A', selector: 'selectAll:' },
        ],
      },
    ]));
  }
});
app.on('activate', createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

function cleanFile(file) {
  return {
    name: file.name,
    path: file.path,
    length: file.length,
    done: file.done,
    downloaded: file.downloaded,
  };
}

function cleanTorrent(torrent) {
  return {
    infoHash: torrent.infoHash,
    name: torrent.name,
    length: torrent.length,
    done: torrent.done,
    progress: torrent.progress,
    downloaded: torrent.downloaded,
    uploaded: torrent.uploaded,
    timeRemaining: torrent.timeRemaining,
    files: torrent.files.map(cleanFile).sort((a, b) => {
      const aSegments = a.path.split('/');
      const bSegments = b.path.split('/');

      for (let i = 0; i < Math.min(aSegments.length, bSegments.length); i++) {
        const aSeg = aSegments[i];
        const bSeg = bSegments[i];

        if (aSeg.match(/^\d/) && bSeg.match(/^\d/)) {
          const aNum = parseInt(aSeg, 10);
          const bNum = parseInt(bSeg, 10);

          if (aNum !== bNum) {
            return aNum - bNum;
          }
        }

        if (aSeg > bSeg) { return 1; }
        if (aSeg < bSeg) { return -1; }
      }

      if (aSegments.length < bSegments.length) { return 1; }
      if (aSegments.length > bSegments.length) { return 1; }

      if (a.path > b.path) { return 1; }
      if (a.path < b.path) { return -1; }

      return 0;
    }),
  };
}

ipcMain.on('ready', (event) => {
  event.sender.send('ready');
});

const listeners = new Set();

function pushStatus() {
  const torrents = torrentClient.torrents.map(cleanTorrent).sort((a, b) => {
    if (a.name > b.name) {
      return 1;
    } else if (a.name < b.name) {
      return -1;
    }

    return 0;
  });

  for (const listener of listeners) {
    try {
      listener.send('torrents/push', { torrents });
    } catch (e) {
      listeners.delete(listener);
    }
  }
}

setInterval(pushStatus, 1000);

ipcMain.on('torrents/sync', (event) => {
  event.sender.send('torrents/push', { torrents: torrentClient.torrents.map(cleanTorrent) });
});

ipcMain.on('torrents/subscribe', (event) => {
  listeners.add(event.sender);

  event.sender.send('torrents/push', { torrents: torrentClient.torrents.map(cleanTorrent) });
});

ipcMain.on('torrents/unsubscribe', (event) => {
  listeners.delete(event.sender);
});

ipcMain.on('torrents/add', (event, [ src ]) => {
  torrentClient.add(src, (torrent) => {
    if (torrent) {
      fs.writeFileSync(path.join(app.getPath('userData'), torrent.infoHash + '.torrent'), torrent.torrentFile);
    }

    pushStatus();
  });
});

const handles = {};

ipcMain.on('stream/start', (event, [ uniqueId, infoHash, fileName ]) => {
  const torrent = torrentClient.torrents.find((e) => e.infoHash === infoHash);
  if (!torrent) {
    return;
  }

  const file = torrent.files.find((e) => e.path === fileName);
  if (!file) {
    return;
  }

  // throttle transfers to 1MB/s - seems to mitigate a bug in blink where
  // pushing lots of data into a MediaSource buffer too quickly prevents it
  // from processing the metadata correctly
  const stream = file.createReadStream().pipe(throttle(1024 * 1024));

  const handle = { stream, closed: false };

  handles[uniqueId] = handle;

  stream.on('readable', () => {
    while (true) { // eslint-disable-line no-constant-condition
      const chunk = stream.read(1024 * 1024);
      if (chunk === null) {
        break;
      }

      event.sender.send('stream/data', { uniqueId, infoHash, fileName, chunk: chunk.toString('base64') });
    }
  });

  stream.on('end', () => {
    event.sender.send('stream/end', { uniqueId, infoHash, fileName });
    delete handles[uniqueId];
  });
});

ipcMain.on('stream/stop', (event, [ uniqueId ]) => {
  const handle = handles[uniqueId];

  if (handle) {
    handle.closed = true;

    if (typeof handle.destroy === 'function') { handle.destroy(); }
    if (typeof handle.close === 'function') { handle.close(); }

    delete handles[uniqueId];
  }
});

fs.access(app.getPath('userData'), fs.constants.F_OK | fs.constants.X_OK, (accessErr) => {
  if (accessErr) {
    return;
  }

  fs.readdir(app.getPath('userData'), (readErr, files) => {
    if (readErr) {
      throw readErr;
    }

    files.filter((e) => e.match(/\.torrent$/)).forEach((file) => {
      torrentClient.add(path.join(app.getPath('userData'), file));
    });
  });
});
