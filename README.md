# Gazelle Time

## Prerequisites

You're going to need these things:

* [node.js](https://nodejs.org/)
* [yarn](https://yarnpkg.com/)
* a correctly-configured toolchain for building c libraries

## Quick start

```
$ yarn
$ GAZELLE_URL=https://your-favourite-gazelle-site.com yarn start
```

That's all there is to it. Hopefully.

## Screenshots

### Screenshots: Playlist

![Playlist](screenshots/playlist.png)

![Playlist (loading)](screenshots/playlist-loading.png)

![Playlist (playing)](screenshots/playlist-playing.png)

### Screenshots: Torrents

![Torrent Search](screenshots/torrent-search.png)

### Screenshots: Collages

![Collage Search](screenshots/collage-search.png)

![Collage (images)](screenshots/collage-view-images.png)

![Collage (text)](screenshots/collage-view-text.png)

## Known Problems

### Known Problem: Login

The login process is not standardised across gazelle installations. The
default strategy will not work if the login process has been customised for
the site you want to use this app with. For example, several forks of gazelle
have customised two-factor authentication. This will not work correctly
without being modified specifically for those forks.

### Known Problem: Scroll Performance

Scroll performance while playing audio is not amazing. This can be improved in
time.

## Acknowledgements

The default logo was created by Tae S Yang and can be found at
http://www.flaticon.com/free-icon/gazelle_130642.
