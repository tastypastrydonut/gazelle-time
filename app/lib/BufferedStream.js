const { Transform } = require('stream');

class BufferedStream extends Transform {
  constructor(options = {}) {
    super(options);

    this.length = options.length;
    this.buffer = new Buffer([]);
  }

  _transform(chunk, encoding, cb) {
    if (!this.buffer) {
      return cb(null, chunk);
    }

    this.buffer = Buffer.concat([ this.buffer, chunk ]);

    if (this.buffer.length > this.length) {
      this.push(this.buffer);
      this.buffer = null;
    }

    return cb();
  }
}

module.exports = BufferedStream;
